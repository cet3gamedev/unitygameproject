﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipBullet : MonoBehaviour {
	private Enemy target;
	private Ship parent;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		MoveToTarget ();
	}

	public void Initialize(Ship parent){
		this.target = parent.Target;
		this.parent = parent;
	}

	private void MoveToTarget(){
		if (target != null) {
			transform.position = Vector3.MoveTowards (transform.position, target.transform.position, Time.deltaTime * parent.BulletSpeed);
		}
		if (target == null) {
			Destroy (this.gameObject);
		}
	}

	private void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Enemy") {
			Destroy (this.gameObject);
		}
	}
}
