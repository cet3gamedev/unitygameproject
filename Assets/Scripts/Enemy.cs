﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {
	public float speed = 3.0f;
	private Rigidbody2D rb;
	private Vector2 screenBounds;
	private int life = 10;

	// Use this for initialization
	void Start () {
		rb = this.GetComponent<Rigidbody2D> ();
		rb.velocity = new Vector2 (0, -speed);
		screenBounds = Camera.main.ScreenToWorldPoint (new Vector3 (Screen.width, Screen.height, Camera.main.transform.position.z));
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.position.y < -screenBounds.y * 1.5) {
			Destroy (this.gameObject);
		}
	}

	public bool IsActive{ get; set;}

	private void OnTriggerEnter2D(Collider2D other){
		
		if (other.tag == "Bullet") {
			life--;
		}
		if (life == 0) {
			Score.scoreValue++;
			Destroy (this.gameObject);
		}
		if (other.tag == "Ship") {
			Destroy (this.gameObject);
		}
	}
}
