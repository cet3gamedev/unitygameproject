﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDeploy : MonoBehaviour {
	public GameObject asteroidPrefab;
	public float respawnTime = 2.0f;
	private Vector2 screenBounds;
	private int count = 0;

	// Use this for initialization
	void Start () {
		screenBounds = Camera.main.ScreenToWorldPoint (new Vector3 (Screen.width, Screen.height, Camera.main.transform.position.z));
		StartCoroutine (enemyWave ());
	}

	private void spawnEnemy(){
		GameObject a = Instantiate (asteroidPrefab) as GameObject;
		a.transform.position = new Vector2 (Random.Range (-screenBounds.x, screenBounds.x), screenBounds.y * 2);
	}

	IEnumerator enemyWave(){
		while (true) {
			if ((count > 5) && !(respawnTime < 1.6f)) {
				respawnTime -= 0.1f;
				count = 0;
			}
			yield return new WaitForSeconds (respawnTime);
			spawnEnemy ();
			count++;
		}
	}
}
