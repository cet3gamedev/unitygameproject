﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : MonoBehaviour {
	private Enemy asteroid;
	private Queue<Enemy> enemies = new Queue<Enemy>();
	private float shootTimer;
	private float bulletSpeed = 100.0f;

	public float BulletSpeed{ get{ return bulletSpeed; } }
	public Enemy Target { get{ return asteroid; }}

	[SerializeField]
	private ShipBullet bullet;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown (0)) {
			Attack ();
		}
	}

	private void Attack(){
		if (asteroid == null && enemies.Count > 0) {
			asteroid = enemies.Dequeue ();
		}

		if (asteroid != null) {
			makeBullet ();
		}
	}

	private void makeBullet(){
		ShipBullet c = Instantiate(bullet) as ShipBullet;
		c.transform.position = transform.position;
		c.Initialize (this);
	}

	public void OnTriggerEnter2D(Collider2D target){
		if (target.tag == "Enemy") {
			enemies.Enqueue (target.GetComponent<Enemy> ());
		}
	}

	public void OnTriggerExit2D(Collider2D target){
		if (target.tag == "Enemy") {
			asteroid = null;
		}
	}
}
