﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour {

	[SerializeField]
	Transform rotationCenter;

	[SerializeField]
	float rotationRadius = 0.1f, angularSpeed = 10.0f;

	float posX, posY, angle = 360f;
	// Update is called once per frame
	void Update () {
		posX = rotationCenter.position.x + Mathf.Cos (angle) * rotationRadius;
		posY = rotationCenter.position.y + Mathf.Sin (angle) * rotationRadius;
		transform.position = new Vector2 (posX, posY);
		angle = angle - Time.deltaTime * angularSpeed;

		if (angle <= 0f) {
			angle = 360.0f;
		}
	}
}
