﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Enemy") {
			this.gameObject.SetActive (false);
			Destroy (this.gameObject);
			FindObjectOfType<GameManager> ().EndGame();
		}
	}
}
