﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Range : MonoBehaviour {
	private Enemy asteroid;
	private Queue<Enemy> enemies = new Queue<Enemy>();
	private float shootTimer;
	private float shootCooldown = 0.5f;
	private float bulletSpeed = 30.0f;

	public float BulletSpeed{ get{ return bulletSpeed; } }
	public Enemy Target { get{ return asteroid; }}

	private bool canAttack = true;

	[SerializeField]
	private Bullet bullet;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		Attack ();
	}

	private void Attack(){
		if (!canAttack) {
			shootTimer += Time.deltaTime;

			if (shootTimer >= shootCooldown) {
				canAttack = true;
				shootTimer = 0.0f;
			}
		}

		if (asteroid == null && enemies.Count > 0) {
			asteroid = enemies.Dequeue ();
		}

		if (asteroid != null) {
			if (canAttack) {
				makeBullet ();
				canAttack = false;
			}
		}
	}

	private void makeBullet(){
		Bullet b = Instantiate(bullet) as Bullet;
		b.transform.position = transform.position;
		b.Initialize (this);
	}

	public void OnTriggerEnter2D(Collider2D target){
		if (target.tag == "Enemy") {
			enemies.Enqueue (target.GetComponent<Enemy> ());
		}
	}

	public void OnTriggerExit2D(Collider2D target){
		if (target.tag == "Enemy") {
			asteroid = null;
		}
	}
}
